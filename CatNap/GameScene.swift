import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
   
    var bed = SKSpriteNode()
    var cat = SKSpriteNode()
    
    
    var gameInProcess = true
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        if(self.gameInProcess == false){return}
        
        
      
        
        let collision =
            contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        if (collision == PhysicsCategory.Cat | PhysicsCategory.Bed) {
            playerWin()
        } else if (collision == PhysicsCategory.Cat | PhysicsCategory.Floor) {
            playerLose()
        }
    }
    
    
    func playerWin(){
        self.gameInProcess = false
        
        let message = SKLabelNode(text: "YOU WIN!")
        message.fontColor = UIColor.black
        message.fontSize = 100
        message.fontName = "AvenirNext-Bold"
        message.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        addChild(message)
        
    
        // SHOW CAT FALLING SLEEP ANIMATION
        let fallingAsleepAnimation = self.createSleepyCatAnimation()
        self.cat.run(fallingAsleepAnimation)
        
        
        // PLAY MUSIC
         self.cat.run(SKAction.playSoundFileNamed("Sounds/win.mp3", waitForCompletion: false))
        
        // RESTART GAME
        perform(#selector(GameScene.restartGame), with: nil, afterDelay: 3)
    }
    
    func playerLose(){
        self.gameInProcess = false
        
        let message = SKLabelNode(text: "YOU LOSE!")
        message.fontColor = UIColor.black
        message.fontSize = 100
        message.fontName = "AvenirNext-Bold"
        message.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        addChild(message)
        
        
        // PLAY MUSIC
        self.cat.run(SKAction.playSoundFileNamed("Sounds/lose.mp3", waitForCompletion: false))
        
        // RESTART GAME
        perform(#selector(GameScene.restartGame), with: nil, afterDelay: 3)
    }
    
    @objc func restartGame(){
        self.gameInProcess = true
        let scene = GameScene(fileNamed: "GameScene")
        scene?.scaleMode = scaleMode
        view!.presentScene(scene)
    }
    
    
    
    override func didMove(to view: SKView) {
    

        
        // Calculate playable margin
        let maxAspectRatio: CGFloat = 16.0/9.0
        let maxAspectRatioHeight = size.width / maxAspectRatio
        let playableMargin: CGFloat = (size.height
            - maxAspectRatioHeight)/2
        let playableRect = CGRect(x: 0, y: playableMargin,
                                  width: size.width, height: size.height-playableMargin*2)
        physicsBody = SKPhysicsBody(edgeLoopFrom: playableRect)
        
        // PLAY MUSIC
        self.run(SKAction.playSoundFileNamed("Sounds/backgroundMusic.mp3", waitForCompletion: false))
        
        
        // ADD HITBOXS
        
        // 1. BED
        bed = self.childNode(withName:"bed") as! SKSpriteNode
        let bedBodySize = CGSize(width: 40, height: 30)
        bed.physicsBody = SKPhysicsBody(rectangleOf: bedBodySize)
        bed.physicsBody!.isDynamic = false
        
        
        // 2. CAT
        cat = self.childNode(withName:"cat") as! SKSpriteNode
        let catBodyTexture = SKTexture(imageNamed: "cat_body_outline")
        cat.physicsBody = SKPhysicsBody(texture: catBodyTexture, size: catBodyTexture.size())
        
        
        // PUTTING INTO CATEGORY
        cat.physicsBody?.categoryBitMask = PhysicsCategory.Cat
        bed.physicsBody?.categoryBitMask = PhysicsCategory.Bed
        self.physicsBody?.categoryBitMask = PhysicsCategory.Floor
        
        
        // RULES FOR CAT COLLISION
       cat.physicsBody!.collisionBitMask = PhysicsCategory.Block | PhysicsCategory.Floor | PhysicsCategory.Bed
        
        // add collision NOTIFICATION rules
        cat.physicsBody!.contactTestBitMask = PhysicsCategory.Bed | PhysicsCategory.Floor
        
        
        
       self.physicsWorld.contactDelegate = self as SKPhysicsContactDelegate
        
    }
    
    
    

    
    // DETECT TOUCH
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let touch = touches.first else {
            // if the touch results in a null value, then exit
            return
        }
        
        // get the sprite that was touched
        let positionInScene = touch.location(in: self)
        var touchedNode = self.atPoint(positionInScene)
        
        
        // check the name of the sprite that was touched
        if let name = touchedNode.name
        {
            if name == "wood"
            {
              
                print("#T")
                
                touchedNode = touchedNode as! SKSpriteNode
                
                touchedNode.run(SKAction.sequence([
                    SKAction.playSoundFileNamed("Sounds/pop.mp3", waitForCompletion: false),
                    SKAction.scale(by: 0.8, duration: 0.1),
                    SKAction.removeFromParent()]))
            }
            
            
        }

    }
    
    
    // COLLISION
    struct PhysicsCategory {
        static let None:  UInt32 = 0
        static let Cat:   UInt32 = 0b1      // 0x00000001 = 1
        static let Block: UInt32 = 0b10     // 0x00000010 = 2
        static let Bed:   UInt32 = 0b100    // 0x00000100 = 4
        static let Floor: UInt32 = 0b1000   // 0x00001000 = 8
    }

    
    func createSleepyCatAnimation() -> SKAction{
        //animate the cat going to sleep
        let frame1 = SKTexture.init(imageNamed: "cat_curlup1")
        let frame2 = SKTexture.init(imageNamed: "cat_curlup2")
        let frame3 = SKTexture.init(imageNamed: "cat_curlup3")
        let sleepFrames: [SKTexture] = [frame1, frame2,frame3]
        
        // Change the frame per 0.25 sec
        let animation = SKAction.animate(with: sleepFrames, timePerFrame: 0.25)
        return animation
    }

    
    

}
